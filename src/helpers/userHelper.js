export const checkIfUserChanged = (currUser, inputValues) => {
  let isChanged = false;
  const userCandidate = {};

  Object.keys(currUser).forEach((key) => {
    if (inputValues[key] && currUser[key] !== inputValues[key]) {
      userCandidate[key] = inputValues[key];
      isChanged = true;
    }
  });

  return {
    isChanged,
    userCandidate,
  };
};
