export const sortMessagesByDate = (messages) => {
  messages.sort((a, b) => {
    return new Date(a.createdAt) - new Date(b.createdAt);
  });
};

export const createUsersMap = (messages) => {
  const users = new Map();
  messages.forEach((message) => {
    if (!users.has(message.userId)) {
      users.set(message.userId, {
        username: message.user,
        avatar: message.avatar,
      });
    }
  });
  return users;
};

export const getCurrentUserId = (messages) => {
  const numberOfMessage = Math.floor(Math.random() * messages.length);
  return messages[numberOfMessage].userId;
};
