import Layout from "./hoc/Layout/Layout";
import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Chat from "./containers/Chat/Chat";
import LoginPage from "./containers/LoginPage/LoginPage";
import MessageEditor from "./containers/MessageEditor/MessageEditor";
import UserList from "./containers/UserList/UserList";
import UserEditor from "./containers/UserEditor/UserEditor";

const App = (props) => {
  let routes = <Route path="/login" component={LoginPage} />;
  let links = [{ path: "/login", name: "login" }];

  if (props.isAuthenticated) {
    if (props.isAdmin) {
      alert("Entered as admin.");
      routes = (
        <>
          <Route path="/login" component={LoginPage} />
          <Route exact path="/users" component={UserList} />
          <Route exact path="/users/:id" component={UserEditor} />
          <Route path="/new-user" component={UserEditor} />
          <Route exact path="/messages" component={Chat} />
          <Route exact path="/messages/:id" component={MessageEditor} />
        </>
      );
      links = links.concat(
        { path: "/users", name: "users" },
        { path: "/messages", name: "chat" }
      );
    } else {
      alert("Entered as simple user.");
      routes = (
        <>
          <Route path="/login" component={LoginPage} />
          <Route exact path="/messages" component={Chat} />
          <Route exact path="/messages/:id" component={MessageEditor} />
        </>
      );
      links = links.concat({ path: "/messages", name: "chat" });
    }
  }

  return (
    <BrowserRouter>
      <Layout routes={links}>
        <Switch>
          {routes}
          <Redirect to="/login" />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
};

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
    isAuthenticated: state.auth.isAuthenticated,
  };
};

export default connect(mapStateToProps)(App);
