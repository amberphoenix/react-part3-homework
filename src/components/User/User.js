import React from "react";
import { useHistory } from "react-router";
import "./User.css";

const User = ({ user, onDelete, onEdit }) => {
  const history = useHistory();

  return (
    <div className="user">
      <div className="user-info">
        <p className="username">
          {user.firstName} {user.lastName}
        </p>
        <p className="email">{user.email}</p>
      </div>
      <div className="user-buttons">
        <button
          className="user-edit-button"
          onClick={() => {
            onEdit(user);
            history.push(`/users/${user.id}`);
          }}
        >
          EDIT
        </button>
        <button
          className="user-delete-button"
          onClick={() => onDelete(user.id)}
        >
          DELETE
        </button>
      </div>
    </div>
  );
};

export default User;
