import React, { useState } from "react";
import { useHistory } from "react-router";
import { connect } from "react-redux";
import { editUser, createUser } from "../../store/actions/userActions";
import { checkIfUserChanged } from "../../helpers/userHelper";
import "./UserEditor.css";

const UserEditor = (props) => {
  const history = useHistory();

  const [firstNameValue, setFirstNameValue] = useState(props.user.firstName);
  const [lastNameValue, setLastNameValue] = useState(props.user.lastName);
  const [emailValue, setEmailValue] = useState(props.user.email);
  const [phoneNumberValue, setPhoneNumberValue] = useState("");
  const [passwordValue, setPasswordValue] = useState("");

  const onFirstNameInputChange = (event) => {
    setFirstNameValue(event.target.value);
  };

  const onLastNameInputChange = (event) => {
    setLastNameValue(event.target.value);
  };

  const onEmailInputChange = (event) => {
    setEmailValue(event.target.value);
  };

  const onPhoneNumberInputChange = (event) => {
    setPhoneNumberValue(event.target.value);
  };

  const onPasswordInputChange = (event) => {
    setPasswordValue(event.target.value);
  };

  const onSubmitButtonClicked = () => {
    if (props.isEditing) {
      const inputValuesToEdit = {
        firstName: firstNameValue,
        lastName: lastNameValue,
        email: emailValue,
      };

      const { isChanged, userCandidate } = checkIfUserChanged(
        props.user,
        inputValuesToEdit
      );

      if (isChanged) {
        props.editUser({ id: props.user.id, ...userCandidate });
        history.push("/users");
      }
    } else {
      const userCandidate = {
        firstName: firstNameValue,
        lastName: lastNameValue,
        email: emailValue,
        phoneNumber: phoneNumberValue,
        password: passwordValue,
      };

      props.createUser(userCandidate);
      history.push("/users");
    }
  };

  const onCancelButtonClicked = () => {
    history.push("/users");
  };

  return (
    <div className="user-edit">
      <p className="title">User</p>
      <p className="title-of-input">First name:</p>
      <input
        className="username-edit"
        value={firstNameValue}
        onChange={onFirstNameInputChange}
      />
      <p className="title-of-input">Last name:</p>
      <input
        className="username-edit"
        value={lastNameValue}
        onChange={onLastNameInputChange}
      />
      <p className="title-of-input">Email:</p>
      <input
        className="email-edit"
        value={emailValue}
        onChange={onEmailInputChange}
      />
      {props.isEditing ? (
        <></>
      ) : (
        <>
          <p className="title-of-input">Phone number:</p>
          <input
            className="phone-number-edit"
            value={phoneNumberValue}
            onChange={onPhoneNumberInputChange}
          />
          <p className="title-of-input">Password:</p>
          <input
            className="password-edit"
            value={passwordValue}
            onChange={onPasswordInputChange}
          />
        </>
      )}
      <div className="buttons">
        <button className="button save" onClick={onSubmitButtonClicked}>
          SUBMIT
        </button>
        <button className="button close" onClick={onCancelButtonClicked}>
          CANCEL
        </button>
      </div>
      {props.error ? <h1 className="display-center">{props.error}</h1> : <></>}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user.userCandidateToEdit,
    isEditing: state.user.isEditing,
    error: state.user.error,
  };
};

const mapDispatchToProps = {
  editUser,
  createUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserEditor);
