import React, { useEffect } from "react";
import { useHistory } from "react-router";
import { connect } from "react-redux";
import {
  startEditingMessage,
  createMessage,
  deleteMessage,
  likeMessage,
  fetchMessages,
} from "../../store/actions/chatActions";
import Loader from "../../components/Loader/Loader";
import ChatHeader from "../../components/ChatHeader/ChatHeader";
import MessageInput from "../../components/MessageInput/MessageInput";
import MessageList from "../../components/MessageList/MessageList";
import "./Chat.css";

const Chat = (props) => {
  const history = useHistory();

  const onSendMessage = (input) => {
    const message = {
      userId: props.activeUserId,
      avatar: props.participants.get(props.activeUserId).avatar,
      user: props.participants.get(props.activeUserId).username,
      text: input,
      createdAt: new Date(),
      editedAt: "",
      liked: [],
    };

    props.createMessage(message);
  };

  const onDeleteMessage = (messageId) => {
    props.deleteMessage(messageId);
  };

  const onLikeMessage = (messageId) => {
    props.likeMessage(messageId, props.activeUserId);
  };

  const onEditMessage = (messageId) => {
    const currMessage = props.chatMessages.find(({ id }) => id === messageId);
    props.startEditingMessage(currMessage);
    history.push(`/messages/${currMessage.id}`);
  };

  const onKeyArrowUp = (event) => {
    if (event.key !== "ArrowUp") return;
    const activeUserMessages = props.chatMessages.filter(
      (message) => message.userId === props.activeUserId
    );
    if (!activeUserMessages.length) return;
    const lastMessage = activeUserMessages[activeUserMessages.length - 1];
    props.startEditingMessage(lastMessage);
    history.push(`/messages/${lastMessage.id}`);
  };

  useEffect(() => {
    props.fetchMessages();
  }, []);

  if (props.isLoading) return <Loader />;
  if (props.error) return <h1 className="display-center">{props.error}</h1>;

  return (
    <>
      <ChatHeader
        title={"MyChat"}
        participantsNumber={props.participantsCount}
        messagesNumber={props.chatMessagesAmount}
        timeOfLastMessage={props.lastMessageTime}
      />
      <MessageList
        onKeyUp={onKeyArrowUp}
        tabIndex={0}
        activeUser={props.activeUserId}
        messages={props.chatMessages}
        onDelete={onDeleteMessage}
        onLike={onLikeMessage}
        onEdit={onEditMessage}
      />
      <MessageInput onSendButtonClicked={onSendMessage} />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    isLoading: state.chat.isLoading,
    chatMessages: state.chat.messages,
    chatMessagesAmount: state.chat.messagesAmount,
    activeUserId: state.auth.currentUser.id,
    participants: state.chat.participants,
    participantsCount: state.chat.participantsCount,
    lastMessageTime: state.chat.lastMessageTime,
    error: state.chat.error,
  };
};

const mapDispatchToProps = {
  createMessage,
  deleteMessage,
  likeMessage,
  fetchMessages,
  startEditingMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
