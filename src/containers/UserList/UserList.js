import React, { useEffect } from "react";
import { useHistory } from "react-router";
import { connect } from "react-redux";
import {
  fetchUsers,
  startCreatingUser,
  startEditingUser,
  deleteUser,
} from "../../store/actions/userActions";
import User from "../../components/User/User";
import Loader from "../../components/Loader/Loader";
import "./UserList.css";

const UserList = (props) => {
  const history = useHistory();
  const onAddNewUserClicked = () => {
    props.startCreatingUser();
    history.push("/new-user");
  };

  useEffect(() => {
    props.fetchUsers();
  }, []);

  if (props.isLoading) return <Loader />;
  if (props.error) return <h1 className="display-center">{props.error}</h1>;

  return (
    <div>
      <button className="add-user-button" onClick={onAddNewUserClicked}>
        ADD NEW USER
      </button>
      <div className="users-list">
        {props.users.map((user) => {
          return (
            <User
              key={user.id}
              user={user}
              onDelete={props.deleteUser}
              onEdit={props.startEditingUser}
            />
          );
        })}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isLoading: state.user.isLoading,
    users: state.user.users,
    error: state.user.error,
  };
};

const mapDispatchToProps = {
  fetchUsers,
  deleteUser,
  startEditingUser,
  startCreatingUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
