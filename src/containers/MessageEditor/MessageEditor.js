import React, { useState } from "react";
import { useHistory } from "react-router";
import { connect } from "react-redux";
import { editMessage } from "../../store/actions/chatActions";
import "./MessageEditor.css";

const MessageEditor = (props) => {
  const history = useHistory();
  const [textMessageValue, setTextMessageValue] = useState(props.message.text);

  const onTextAreaChange = (event) => {
    setTextMessageValue(event.target.value);
  };

  const onSaveClicked = () => {
    if (textMessageValue !== props.message.text) {
      props.editMessage(props.message.id, textMessageValue);
      history.push("/chat");
    }
  };

  const onCancelClicked = () => {
    history.push("/chat");
  };

  return (
    <div className="message-edit">
      <p className="title">Edit Message</p>
      <textarea
        rows="25"
        cols="60"
        className="edit-input"
        value={textMessageValue}
        onChange={onTextAreaChange}
      />
      <div className="buttons">
        <button className="button save" onClick={onSaveClicked}>
          SAVE
        </button>
        <button className="button close" onClick={onCancelClicked}>
          CANCEL
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    message: state.chat.messageCandidateToEdit,
  };
};

const mapDispatchToProps = {
  editMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageEditor);
