import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";
import { connect } from "react-redux";
import { authLogin } from "../../store/actions/authAction";
import "./LoginPage.css";

const LoginPage = (props) => {
  const history = useHistory();

  const [emailValue, setEmailValue] = useState("");
  const [passwordValue, setPasswordValue] = useState("");

  const onEmailInputChange = (event) => {
    setEmailValue(event.target.value);
  };

  const onPasswordInputChange = (event) => {
    setPasswordValue(event.target.value);
  };

  const onLoginClicked = () => {
    let isAdmin = false;

    if (emailValue === "adminka@gmail.com" && passwordValue === "admin")
      isAdmin = true;

    props.authLogin({ email: emailValue, password: passwordValue }, isAdmin);
  };

  return (
    <div className="login-page">
      <p className="login-title">Log in to enter the Chatto :3</p>
      <span className="name-of-input">Email:</span>
      <input
        className="username-input"
        value={emailValue}
        onChange={onEmailInputChange}
      />
      <span className="name-of-input">Password:</span>
      <input
        type="password"
        className="password-input"
        value={passwordValue}
        onChange={onPasswordInputChange}
      />
      <button className="login-button" onClick={onLoginClicked}>
        LOG IN
      </button>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    currentUser: state.auth.currentUser,
    isAdmin: state.auth.isAdmin,
    isAuthenticated: state.auth.isAuthenticated,
  };
};

const mapDispatchToProps = {
  authLogin,
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
