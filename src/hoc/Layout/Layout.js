import React from "react";
import { NavLink, Router } from "react-router-dom";
import "./Layout.css";

const Layout = (props) => {
  return (
    <>
      <header className="header">
        <i className="fas fa-paw logo"></i>
        Chatto |
        {props.routes.map((route) => {
          if (!route.name) return null;
          return (
            <NavLink key={route.path} className="nav-link" to={route.path}>
              {route.name}
            </NavLink>
          );
        })}
      </header>
      <main className="main">{props.children}</main>
      <footer className="footer">&copy; Copyright</footer>
    </>
  );
};

export default Layout;
