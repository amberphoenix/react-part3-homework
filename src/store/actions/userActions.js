import {
  ADD_USER,
  ADD_USER_ERROR,
  DELETE_USER,
  FETCH_USERS,
  FETCH_USERS_ERROR,
  FETCH_USERS_SUCCESS,
  START_EDITING_USER,
  EDIT_USER,
  EDIT_USER_ERROR,
  START_ADDING_USER,
  DELETE_USER_ERROR,
} from "../actionTypes";

export const fetchUsers = () => ({
  type: FETCH_USERS,
});

export const fetchUsersSuccess = (users) => ({
  type: FETCH_USERS_SUCCESS,
  payload: {
    users,
  },
});

export const fetchUsersError = (error) => ({
  type: FETCH_USERS_ERROR,
  payload: {
    error,
  },
});

export const startCreatingUser = () => ({
  type: START_ADDING_USER,
});

export const createUser = (user) => ({
  type: ADD_USER,
  payload: {
    user,
  },
});

export const createUserError = (error) => ({
  type: ADD_USER_ERROR,
  payload: {
    error,
  },
});

export const deleteUser = (userId) => ({
  type: DELETE_USER,
  payload: {
    userId,
  },
});

export const deleteUserError = (error) => ({
  type: DELETE_USER_ERROR,
  payload: {
    error,
  },
});

export const startEditingUser = (user) => ({
  type: START_EDITING_USER,
  payload: {
    user,
  },
});

export const editUser = (user) => ({
  type: EDIT_USER,
  payload: {
    user,
  },
});

export const editUserError = (error) => ({
  type: EDIT_USER_ERROR,
  payload: {
    error,
  },
});
