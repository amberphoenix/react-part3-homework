import {
  AUTH_LOGIN,
  AUTH_LOGIN_ERROR,
  AUTH_LOGIN_SUCCESS,
} from "../actionTypes";

export const authLogin = (user, isAdmin) => ({
  type: AUTH_LOGIN,
  payload: {
    user,
    isAdmin,
  },
});

export const authLoginSuccess = (user) => ({
  type: AUTH_LOGIN_SUCCESS,
  payload: {
    user,
  },
});

export const authLoginError = (error) => ({
  type: AUTH_LOGIN_ERROR,
  payload: {
    error,
  },
});
