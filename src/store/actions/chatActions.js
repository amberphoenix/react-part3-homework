import {
  CREATE_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
  EDIT_MESSAGE,
  FETCH_MESSAGES,
  FETCH_MESSAGES_SUCCESS,
  FETCH_MESSAGES_ERROR,
  DELETE_MESSAGE_ERROR,
  LIKE_MESSAGE_ERROR,
  CREATE_MESSAGE_ERROR,
  START_EDITING_MESSAGE,
  EDIT_MESSAGE_ERROR,
} from "../actionTypes";

export const fetchMessages = () => ({
  type: FETCH_MESSAGES,
});

export const fetchMessagesSuccess = (messages) => ({
  type: FETCH_MESSAGES_SUCCESS,
  payload: {
    messages,
  },
});

export const fetchMessagesError = (error) => ({
  type: FETCH_MESSAGES_ERROR,
  payload: {
    error,
  },
});

export const createMessage = (message) => ({
  type: CREATE_MESSAGE,
  payload: {
    message,
  },
});

export const createMessageError = (error) => ({
  type: CREATE_MESSAGE_ERROR,
  payload: {
    error,
  },
});

export const deleteMessage = (messageId) => ({
  type: DELETE_MESSAGE,
  payload: {
    messageId,
  },
});

export const deleteMessageError = (error) => ({
  type: DELETE_MESSAGE_ERROR,
  payload: {
    error,
  },
});

export const likeMessage = (messageId, activeUserId) => ({
  type: LIKE_MESSAGE,
  payload: {
    messageId,
    activeUserId,
  },
});

export const likeMessageError = (error) => ({
  type: LIKE_MESSAGE_ERROR,
  payload: {
    error,
  },
});

export const startEditingMessage = (message) => ({
  type: START_EDITING_MESSAGE,
  payload: {
    message,
  },
});

export const editMessage = (messageId, messageText) => ({
  type: EDIT_MESSAGE,
  payload: {
    messageId,
    messageText,
  },
});

export const editMessageError = (error) => ({
  type: EDIT_MESSAGE_ERROR,
  payload: {
    error,
  },
});
