import axios from "axios";
import api from "../../shared/api";
import { call, put, takeEvery, all } from "redux-saga/effects";
import {
  CREATE_MESSAGE,
  CREATE_MESSAGE_ERROR,
  DELETE_MESSAGE,
  DELETE_MESSAGE_ERROR,
  EDIT_MESSAGE,
  EDIT_MESSAGE_ERROR,
  FETCH_MESSAGES,
  FETCH_MESSAGES_ERROR,
  FETCH_MESSAGES_SUCCESS,
  LIKE_MESSAGE,
  LIKE_MESSAGE_ERROR,
} from "../actionTypes";

export function* fetchMessages() {
  try {
    const messages = yield call(axios.get, `${api.url}/messages`);
    yield put({
      type: FETCH_MESSAGES_SUCCESS,
      payload: { messages: messages.data },
    });
  } catch (error) {
    yield put({
      type: FETCH_MESSAGES_ERROR,
      payload: { error: error.message },
    });
  }
}

function* watchFetchMessages() {
  yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

export function* createMessage(action) {
  const newMessage = { ...action.payload.message };

  try {
    yield call(axios.post, `${api.url}/messages`, newMessage);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    yield put({
      type: CREATE_MESSAGE_ERROR,
      payload: { error: error.message },
    });
  }
}

function* watchCreateMessage() {
  yield takeEvery(CREATE_MESSAGE, createMessage);
}

export function* deleteMessage(action) {
  try {
    yield call(axios.delete, `${api.url}/messages/${action.payload.messageId}`);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    yield put({
      type: DELETE_MESSAGE_ERROR,
      payload: { error: error.message },
    });
  }
}

function* watchDeleteMessage() {
  yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

export function* likeMessage(action) {
  try {
    const { data: message } = yield call(
      axios.get,
      `${api.url}/messages/${action.payload.messageId}`
    );
    const likes = message.liked;

    if (likes.includes(action.payload.activeUserId)) {
      likes.splice(likes.indexOf(action.payload.activeUserId), 1);
    } else {
      likes.push(action.payload.activeUserId);
    }

    yield call(axios.put, `${api.url}/messages/${action.payload.messageId}`, {
      liked: likes,
    });

    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    yield put({
      type: LIKE_MESSAGE_ERROR,
      payload: { error: error.message },
    });
  }
}

function* watchLikeMessage() {
  yield takeEvery(LIKE_MESSAGE, likeMessage);
}

export function* editMessage(action) {
  const { messageId, messageText } = action.payload;

  try {
    yield call(axios.put, `${api.url}/messages/${messageId}`, {
      text: messageText,
    });
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    yield put({ type: EDIT_MESSAGE_ERROR, payload: { error: error.message } });
  }
}

function* watchEditMessage() {
  yield takeEvery(EDIT_MESSAGE, editMessage);
}

export default function* messagesSagas() {
  yield all([
    watchFetchMessages(),
    watchDeleteMessage(),
    watchLikeMessage(),
    watchCreateMessage(),
    watchEditMessage(),
  ]);
}
