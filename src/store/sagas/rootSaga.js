import { all } from "redux-saga/effects";
import userSaga from "./userSaga";
import chatSaga from "./chatSaga";
import authSaga from "./authSaga";

export default function* rootSaga() {
  yield all([chatSaga(), userSaga(), authSaga()]);
}
