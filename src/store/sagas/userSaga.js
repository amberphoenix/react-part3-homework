import axios from "axios";
import api from "../../shared/api";
import { call, put, takeEvery, all } from "redux-saga/effects";
import {
  FETCH_USERS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_ERROR,
  ADD_USER,
  EDIT_USER,
  DELETE_USER,
  DELETE_USER_ERROR,
  EDIT_USER_ERROR,
  ADD_USER_ERROR,
} from "../actionTypes";

export function* fetchUsers() {
  try {
    const users = yield call(axios.get, `${api.url}/users`);
    yield put({ type: FETCH_USERS_SUCCESS, payload: { users: users.data } });
  } catch (error) {
    yield put({ type: FETCH_USERS_ERROR, payload: { error: error.message } });
  }
}

function* watchFetchUsers() {
  yield takeEvery(FETCH_USERS, fetchUsers);
}

export function* addUser(action) {
  const newUser = { ...action.payload.user };

  try {
    yield call(axios.post, `${api.url}/users`, newUser);
    yield put({ type: FETCH_USERS });
  } catch (error) {
    yield put({ type: ADD_USER_ERROR, payload: { error: error.message } });
  }
}

function* watchAddUser() {
  yield takeEvery(ADD_USER, addUser);
}

export function* updateUser(action) {
  const { id, ...updatedUser } = action.payload.user;

  try {
    yield call(axios.put, `${api.url}/users/${id}`, updatedUser);
    yield put({ type: FETCH_USERS });
  } catch (error) {
    yield put({ type: EDIT_USER_ERROR, payload: { error: error.message } });
  }
}

function* watchUpdateUser() {
  yield takeEvery(EDIT_USER, updateUser);
}

export function* deleteUser(action) {
  try {
    yield call(axios.delete, `${api.url}/users/${action.payload.userId}`);
    yield put({ type: FETCH_USERS });
  } catch (error) {
    yield put({ type: DELETE_USER_ERROR, payload: { error: error.message } });
  }
}

function* watchDeleteUser() {
  yield takeEvery(DELETE_USER, deleteUser);
}

export default function* usersSagas() {
  yield all([
    watchFetchUsers(),
    watchAddUser(),
    watchUpdateUser(),
    watchDeleteUser(),
  ]);
}
