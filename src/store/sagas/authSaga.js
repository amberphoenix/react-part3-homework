import axios from "axios";
import api from "../../shared/api";
import { call, put, takeEvery, all } from "redux-saga/effects";
import {
  AUTH_LOGIN,
  AUTH_LOGIN_ERROR,
  AUTH_LOGIN_SUCCESS,
} from "../actionTypes";

export function* authLogin(action) {
  try {
    const userCredentials = { ...action.payload.user };
    console.log(userCredentials);
    const user = yield call(
      axios.post,
      `${api.url}/auth/login`,
      userCredentials
    );
    yield put({ type: AUTH_LOGIN_SUCCESS, payload: { user: user.data } });
  } catch (error) {
    alert("Invalid user credentials");
    yield put({
      type: AUTH_LOGIN_ERROR,
      payload: { error: error.message },
    });
  }
}

function* watchAuthLogin() {
  yield takeEvery(AUTH_LOGIN, authLogin);
}

export default function* authSaga() {
  yield all([watchAuthLogin()]);
}
