import { combineReducers } from "redux";
import chatReducer from "./chatReducer";
import userReducer from "./userReducer";
import authReducer from "./authReducer";

const rootReducer = combineReducers({
  chat: chatReducer,
  user: userReducer,
  auth: authReducer,
});

export default rootReducer;
