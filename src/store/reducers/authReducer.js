import {
  AUTH_LOGIN,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_ERROR,
} from "../actionTypes";

const initialState = {
  currentUser: {},
  isAdmin: false,
  isAuthenticated: false,
  isLoading: false,
  error: "",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case AUTH_LOGIN: {
      const { user, isAdmin } = action.payload;
      return {
        ...state,
        isAdmin,
        isLoading: true,
      };
    }

    case AUTH_LOGIN_SUCCESS: {
      const { user } = action.payload;

      return {
        ...state,
        currentUser: user,
        isAuthenticated: true,
        isLoading: false,
      };
    }

    case AUTH_LOGIN_ERROR: {
      const { error } = action.payload;

      return {
        ...state,
        error,
        isAuthenticated: false,
        isLoading: false,
      };
    }

    default:
      return state;
  }
}
