import {
  DELETE_USER_ERROR,
  EDIT_USER_ERROR,
  EDIT_USER,
  FETCH_USERS,
  FETCH_USERS_ERROR,
  FETCH_USERS_SUCCESS,
  START_EDITING_USER,
  START_ADDING_USER,
} from "../actionTypes";

const initialState = {
  users: [],
  userCandidateToEdit: {},
  isEditing: false,
  isLoading: false,
  error: "",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_USERS: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case FETCH_USERS_SUCCESS: {
      const { users } = action.payload;

      return {
        ...state,
        users,
        isLoading: false,
      };
    }

    case FETCH_USERS_ERROR: {
      const { error } = action.payload;

      return {
        ...state,
        error: error,
        isLoading: false,
      };
    }

    case START_ADDING_USER: {
      return {
        ...state,
        isEditing: false,
      };
    }

    case START_EDITING_USER: {
      const { user: userCandidateToEdit } = action.payload;
      return {
        ...state,
        userCandidateToEdit,
        isEditing: true,
      };
    }

    case EDIT_USER: {
      return {
        ...state,
        userCandidateToEdit: {},
        isEditing: false,
      };
    }

    case EDIT_USER_ERROR: {
      const { error } = action.payload;

      return {
        ...state,
        error,
      };
    }

    case DELETE_USER_ERROR: {
      const { error } = action.payload;

      return {
        ...state,
        error: error,
      };
    }

    default:
      return state;
  }
}
