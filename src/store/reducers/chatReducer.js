import {
  createUsersMap,
  sortMessagesByDate,
  getCurrentUserId,
} from "../../helpers/chatHelpers";
import {
  CREATE_MESSAGE,
  DELETE_MESSAGE,
  FETCH_MESSAGES,
  FETCH_MESSAGES_SUCCESS,
  FETCH_MESSAGES_ERROR,
  DELETE_MESSAGE_ERROR,
  CREATE_MESSAGE_ERROR,
  EDIT_MESSAGE_ERROR,
  START_EDITING_MESSAGE,
} from "../actionTypes";

const initialState = {
  isLoading: false,
  messages: [],
  messagesAmount: 0,
  participants: {},
  participantsCount: 0,
  lastMessageTime: "00:00",
  messageCandidateToEdit: {},
  error: "",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_MESSAGES: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case FETCH_MESSAGES_SUCCESS: {
      const { messages } = action.payload;

      sortMessagesByDate(messages);
      const messagesAmount = messages.length;

      const lastMessageDate = messages[messagesAmount - 1].createdAt;
      const lastMessageTime = `${new Date(
        lastMessageDate
      ).getHours()}:${new Date(lastMessageDate).getMinutes()}`;

      const participants = createUsersMap(messages);
      const participantsCount = participants.size;

      return {
        ...state,
        messages,
        messagesAmount,
        participants,
        participantsCount,
        lastMessageTime,
        isLoading: false,
      };
    }

    case FETCH_MESSAGES_ERROR: {
      const { error } = action.payload;
      return {
        ...state,
        error,
        isLoading: false,
      };
    }

    case CREATE_MESSAGE: {
      const { message } = action.payload;

      const lastMessageTime = `${new Date(
        message.createdAt
      ).getHours()}:${new Date(message.createdAt).getMinutes()}`;
      const messagesAmount = state.messagesAmount + 1;

      return {
        ...state,
        messagesAmount,
        lastMessageTime,
      };
    }

    case CREATE_MESSAGE_ERROR: {
      const { error } = action.payload;

      return {
        ...state,
        error,
      };
    }

    case DELETE_MESSAGE: {
      const messagesAmount = state.messages.length;
      const lastMessageDate = state.messages[messagesAmount - 1].createdAt;
      const lastMessageTime = `${new Date(
        lastMessageDate
      ).getHours()}:${new Date(lastMessageDate).getMinutes()}`;

      return {
        ...state,
        messagesAmount,
        lastMessageTime,
      };
    }

    case DELETE_MESSAGE_ERROR: {
      const { error } = action.payload;

      return {
        ...state,
        error,
      };
    }

    case START_EDITING_MESSAGE: {
      const { message } = action.payload;

      return {
        ...state,
        messageCandidateToEdit: message,
      };
    }

    case EDIT_MESSAGE_ERROR: {
      const { error } = action.payload;

      return {
        ...state,
        error,
      };
    }

    default:
      return state;
  }
}
