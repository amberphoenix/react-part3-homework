const messageRoutes = require("./messageRoutes");
const userRoutes = require("./userRoutes");
const authRoutes = require("./authRoutes");

module.exports = (app) => {
  app.use("/api/messages", messageRoutes);
  app.use("/api/users", userRoutes);
  app.use("/api/auth", authRoutes);
};
