const { Router } = require("express");
const { responseMiddleware } = require("../middlewares/response.middleware");
const { userLogin } = require("../controllers/authControllers");

const router = Router();

router.post("/login", userLogin, responseMiddleware);

module.exports = router;
