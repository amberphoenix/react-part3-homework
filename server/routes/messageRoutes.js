const { Router } = require("express");
const {
  getMessages,
  getMessageById,
  createMessage,
  updateMessage,
  deleteMessage,
} = require("../controllers/messageController");
const {
  createMessageValid,
  updateMessageValid,
} = require("../middlewares/message.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.get("/", getMessages, responseMiddleware);
router.get("/:id", getMessageById, responseMiddleware);
router.post("/", createMessageValid, createMessage, responseMiddleware);
router.put("/:id", updateMessageValid, updateMessage, responseMiddleware);
router.delete("/:id", deleteMessage, responseMiddleware);

module.exports = router;
