module.exports = {
  email: /^[a-z0-9](.?[a-z0-9]){5,}@gmail.com$/,
  phoneNumber: /^\+380([0-9]{9})$/,
};
