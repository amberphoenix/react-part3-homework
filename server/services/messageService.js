const { MessageRepository } = require("../repositories/messageRepository");
const ErrorWithStatusCode = require("../helpers/ErrorWithStatusCode");
const statusCodes = require("../constants/statusCodes");

class MessageService {
  getAll() {
    const messages = MessageRepository.getAll();
    if (!messages.length) {
      throw new ErrorWithStatusCode(
        "No messages in database",
        statusCodes.notFound
      );
    }
    return { data: messages, statusCode: statusCodes.ok };
  }

  getById(id) {
    const message = MessageRepository.getOne(id);
    if (!message) {
      throw new ErrorWithStatusCode("Message not found", statusCodes.notFound);
    }
    return { data: message, statusCode: statusCodes.ok };
  }

  create(data) {
    const newMessage = MessageRepository.create(data);
    if (!newMessage) {
      throw new ErrorWithStatusCode(
        "Failed to create new message",
        statusCodes.notValid
      );
    }
    return { data: newMessage, statusCode: statusCodes.ok };
  }

  update(id, dataToUpdate) {
    const updatedMessage = MessageRepository.update(id, dataToUpdate);
    if (!updatedMessage) {
      throw new ErrorWithStatusCode(
        "Failed to update message",
        statusCodes.notValid
      );
    }
    return { data: updatedMessage, statusCode: statusCodes.ok };
  }

  delete(id) {
    const deletedMessage = MessageRepository.delete(id);
    if (!deletedMessage) {
      throw new ErrorWithStatusCode(
        "Failed to delete message",
        statusCodes.notValid
      );
    }
    return { data: deletedMessage, statusCode: statusCodes.ok };
  }
}

module.exports = new MessageService();
