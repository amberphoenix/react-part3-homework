const { message } = require("../models/message");
const {
  containsAllFields,
  hasExtraFields,
  hasEmptyFields,
} = require("../helpers/validators");

const createMessageValid = (req, res, next) => {
  try {
    const messageCandidate = req.body;
    containsAllFields(messageCandidate, message);
    hasExtraFields(messageCandidate, message);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateMessageValid = (req, res, next) => {
  try {
    const messageCandidate = req.body;
    hasExtraFields(messageCandidate, message);
    hasEmptyFields(messageCandidate, message);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createMessageValid = createMessageValid;
exports.updateMessageValid = updateMessageValid;
