const { user } = require("../models/user");
const {
  containsAllFields,
  hasExtraFields,
  hasEmptyFields,
  unique,
  password,
  email,
  phoneNumber,
} = require("../helpers/validators");
const userService = require("../services/userService");

const createUserValid = (req, res, next) => {
  try {
    const userCandidate = req.body;
    containsAllFields(userCandidate, user);
    hasExtraFields(userCandidate, user);
    hasEmptyFields(userCandidate);
    unique(userService.search, "email", userCandidate.email);
    unique(userService.search, "phoneNumber", userCandidate.phoneNumber);
    password(userCandidate.password);
    email(userCandidate.email);
    phoneNumber(userCandidate.phoneNumber);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateUserValid = (req, res, next) => {
  try {
    const userCandidate = req.body;
    hasExtraFields(userCandidate, user);
    hasEmptyFields(userCandidate);
    if (userCandidate.password) password(userCandidate.password);
    if (userCandidate.email) {
      unique(userService.search, "email", userCandidate.email);
      email(userCandidate.email);
    }
    if (userCandidate.phoneNumber) {
      unique(userService.search, "phoneNumber", userCandidate.phoneNumber);
      phoneNumber(userCandidate.phoneNumber);
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
