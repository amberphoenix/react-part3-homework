const AuthService = require("../services/authService");

const userLogin = (req, res, next) => {
  try {
    const userCredentials = {
      email: req.body.email,
      password: req.body.password,
    };
    const { data, statusCode } = AuthService.login(userCredentials);
    res.data = data;
    res.statusCode = statusCode;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.userLogin = userLogin;
