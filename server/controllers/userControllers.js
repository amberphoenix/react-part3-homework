const UserService = require("../services/userService");

exports.getUsers = (req, res, next) => {
  try {
    const { data, statusCode } = UserService.getAll();
    res.data = data;
    res.statusCode = statusCode;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.getUserById = (req, res, next) => {
  try {
    const id = { id: req.params.id };
    const { data, statusCode } = UserService.getById(id);
    res.data = data;
    res.statusCode = statusCode;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createUser = (req, res, next) => {
  if (!res.err) {
    try {
      const { data, statusCode } = UserService.create(req.body);
      res.data = data;
      res.statusCode = statusCode;
    } catch (err) {
      res.err = err;
    }
  }
  next();
};

exports.updateUser = (req, res, next) => {
  if (!res.err) {
    try {
      const { data, statusCode } = UserService.update(req.params.id, req.body);
      res.data = data;
      res.statusCode = statusCode;
    } catch (err) {
      res.err = err;
    }
  }
  next();
};

exports.deleteUser = (req, res, next) => {
  try {
    const { data, statusCode } = UserService.delete(req.params.id);
    res.data = data;
    res.statusCode = statusCode;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};
