const MessageService = require("../services/messageService");

exports.getMessages = (req, res, next) => {
  try {
    const { data, statusCode } = MessageService.getAll();
    res.data = data;
    res.statusCode = statusCode;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.getMessageById = (req, res, next) => {
  try {
    const id = { id: req.params.id };
    const { data, statusCode } = MessageService.getById(id);
    res.data = data;
    res.statusCode = statusCode;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createMessage = (req, res, next) => {
  if (!res.err) {
    try {
      const { data, statusCode } = MessageService.create(req.body);
      res.data = data;
      res.statusCode = statusCode;
    } catch (err) {
      res.err = err;
    }
  }
  next();
};

exports.updateMessage = (req, res, next) => {
  if (!res.err) {
    try {
      const { data, statusCode } = MessageService.update(
        req.params.id,
        req.body
      );
      res.data = data;
      res.statusCode = statusCode;
    } catch (err) {
      res.err = err;
    }
  }
  next();
};

exports.deleteMessage = (req, res, next) => {
  try {
    const { data, statusCode } = MessageService.delete(req.params.id);
    res.data = data;
    res.statusCode = statusCode;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};
